/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package a06t04;

/**
 *
 * @author pc
 */
public class A06T04 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String nomedia = semana(1);
        System.out.println(nomedia);
    }
    public static String semana(int dia){
        String nomedia;
        switch (dia) {
            case 1:
                nomedia = "Domingo";
                break;
            case 2:
                nomedia = "Segunda-feira";
                break;
            case 3:
                nomedia = "Terça-feira";
                break;
            case 4:
                nomedia = "Quarta-feira";
                break;
            case 5:
                nomedia = "Quinta-feira";
                break;
            case 6:
                nomedia = "Sexta-feira";
                break;
             case 7:
                nomedia = "Sábado";
                break;
            default:
                nomedia = "Este não é um dia válido!";
         }
        return nomedia;
    } 
    
}
